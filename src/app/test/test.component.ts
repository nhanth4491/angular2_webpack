import { Component, OnInit } from '@angular/core';

@Component({
    moduleId: module.id,
    selector: 'test',
    templateUrl: '<div>This is test component</div>',
})
export class TestComponent implements OnInit {

    constructor() {
    }

    ngOnInit(): void {
        console.log('test component initializing...');
    }

}